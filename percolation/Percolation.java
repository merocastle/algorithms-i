/*----------------------------------------------------------------
* Author: Shikhar Subedi
* Written: 09/25/2013
*----------------------------------------------------------------*/

public class Percolation {
    private int totalsize;
    private int matrixsize;
    private boolean[] matrix;
    private static final int virtualSites = 2;
    private WeightedQuickUnionUF grid;
    private WeightedQuickUnionUF tempgrid;
    public Percolation(int N)             // create N-by-N grid, with all sites blocked
    {
        matrixsize = N;
        totalsize = matrixsize*matrixsize + virtualSites;
        tempgrid = new WeightedQuickUnionUF(matrixsize*matrixsize+1);
        matrix = new boolean[totalsize];
        grid = new WeightedQuickUnionUF(totalsize);
        matrix[0]=true;
        matrix[totalsize-1]=true;
        for(int i=1;i<(totalsize-1);i++)
        {
            matrix[i]=false;
        }
        
   }    
   public void open(int i, int j)         // open site (row i, column j) if it is not already
   {
       int p = getIndex(i,j);
       boolean check = true;
      
       if(!matrix[p]){
           check = false;
           matrix[p]= true;
       }
       
       if(check)
           return;
       
       if(i==1){
           grid.union(0,p);
           tempgrid.union(0,p);
       }
       
       if(i==matrixsize){
           grid.union(totalsize-1,p);
       }
       
       //now connect the neighboring sites to p
       //first connect to left site j-1
       if(j>1 && isOpen(i,j-1)){
           int left = getIndex(i,j-1);
           grid.union(p,left);
           tempgrid.union(p,left);
       }
       
       // connect to right site j+1
       if(j<matrixsize && isOpen(i,j+1)){
           int right = getIndex(i,j+1);
           grid.union(p,right);
           tempgrid.union(p,right);
           
       }
 
       //connect to top site i-1
       if(i>1 && isOpen(i-1,j)){
           int top = getIndex(i-1,j);
           grid.union(p,top);
           tempgrid.union(p,top);
       }
       
       //connect to bottom site i+1
       if(i<matrixsize && isOpen(i+1,j)){
           int bottom = getIndex(i+1,j);
           grid.union(p,bottom);
           tempgrid.union(p,bottom);
       }
   }
   
   public boolean isOpen(int i, int j)    // is site (row i, column j) open?
   {
       int p = getIndex(i,j);
       return matrix[p];
   }
   
   public boolean isFull(int i, int j)    // is site (row i, column j) full?
   {
       int p = getIndex(i,j);
       if(percolates())
       {
           return ((isOpen(i,j) && tempgrid.connected(p,0)));
           
       }else{
           return (isOpen(i,j) && grid.connected(p,0));
       }
   }
   
   public boolean percolates()            // does the system percolate?
   {
      return grid.connected(0,totalsize-1);
   } 
       
   private int getIndex(int i, int j)
   {
       if(i<1 || i>matrixsize){
           throw new java.lang.IndexOutOfBoundsException("index i out of bounds");
       }
       if(j<1 || j>matrixsize){
           throw new java.lang.IndexOutOfBoundsException("index j out of bounds");
       }
       return (matrixsize*(i-1) +j);
   }
}
