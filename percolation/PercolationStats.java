/*----------------------------------------------------------------
* Author: Shikhar Subedi
* Written: 09/25/2013
* 
*
* Dependencies: Percolation.java
*
* 
*
*----------------------------------------------------------------*/


public class PercolationStats {
    private double[] thresholds;
    public PercolationStats(int N, int T)    // perform T independent computational experiments on an N-by-N grid
    {
        int openedSites;
        
        if(N<=0 || T<=0){
            throw new java.lang.IllegalArgumentException("n or t less than or equal to zero");         
        }
       this.thresholds = new double[T];
       
       for(int i = 0 ;i < T ; i++){
           Percolation percolation = new Percolation(N);
           openedSites =0;
           
           while(true){
            
            int rowIndex=StdRandom.uniform(N)+1;
            int colIndex=StdRandom.uniform(N)+1;
            
            if(!percolation.isOpen(rowIndex,colIndex)){
                
                percolation.open(rowIndex,colIndex);
                
                openedSites++;
                
                if(percolation.percolates()){
                    double th = ((double)(openedSites))/((double)(N*N)) ;
                    thresholds[i]= th;
                    break;
                    
                }
            }
               
           }
       }
    }
    
    public double mean()                     // sample mean of percolation threshold
    {
        return StdStats.mean(thresholds);
    }
    
    public double stddev()                   // sample standard deviation of percolation threshold
    {
        return StdStats.stddev(thresholds);
    }
    
    public double confidenceLo()             // returns lower bound of the 95% confidence interval
    {
        double mean = mean();
        
        double stddev = stddev();
        
        double low = mean - (1.96 *stddev)/Math.sqrt(thresholds.length);
        
        return low;
    }
    
    public double confidenceHi()             // returns upper bound of the 95% confidence interval
    {
         double mean = mean();
        
        double stddev = stddev();
        
        double high = mean + (1.96 *stddev)/Math.sqrt(thresholds.length);
        
        return high;
        
    }
    
    public static void main(String[] args)   // test client, described below
    {
        if(args.length != 2){
            throw new java.lang.IllegalArgumentException("provide two arguments");          
        }
        
        int n = Integer.parseInt(args[0]);
        
        int t = Integer.parseInt(args[1]);
        
        PercolationStats ps = new PercolationStats(n,t);
        
        StdOut.println("mean\t\t\t\t = "+ps.mean());
        
        StdOut.println("stddev\t\t\t\t = "+ps.stddev());
        
        StdOut.println("95% confidence interval\t = "+ps.confidenceLo()+" , "+ps.confidenceHi());;
        
    }
}
