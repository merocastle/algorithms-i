
import java.util.Iterator;

public class RandomizedQueue<Item> implements Iterable<Item> {
    
    private Item[] values; //array to hold the items
    
    private int N; //number of items in the rqueue
    
    public RandomizedQueue()           // construct an empty randomized queue
    {
       this.values = (Item[]) new Object[1];
       
       this.N = 0;
    }
    
    private void resize(int capacity)
    {
        int i;
        Item[] temp = (Item[]) new Object[capacity];
        
        for(i = 0;i<N;i++){
            temp[i] = this.values[i];
        }
        this.values = temp;
    }
    
    public boolean isEmpty()           // is the queue empty?
    {
        return this.N == 0;
    }
    
    public int size()                  // return the number of items on the queue
    {
        return this.N;
    }
    
    public void enqueue(Item item)     // add the item
    {
        if(item == null){
            throw new java.lang.NullPointerException();
        }
        
        int qsize = this.values.length;
        
        if(this.N == qsize){
           this.resize(this.values.length * 2);
        }
        
        this.values[this.N++]= item;
    }
    
    public Item dequeue()              // delete and return a random item
    {
        if(this.N == 0){
            throw new java.util.NoSuchElementException();
        }
        
        int index = StdRandom.uniform(this.N);
        
        Item item = this.values[index];
        
        this.values[index]  = this.values[this.N-1];
        
        this.values[this.N-1] = null;
        
        this.N--;
        
        int qsize = this.values.length;
        
        if(this.N> 0 && this.N <= (qsize/4))
        {
            this.resize(qsize/2);
            
        }
        
        return item;
        
        
    }
    
    
    public Item sample()               // return (but do not delete) a random item
    {
       if(this.N == 0){
            throw new java.util.NoSuchElementException();
        }
       
       int index = StdRandom.uniform(this.N);
       
       Item item = this.values[index];
       
       return item;
       
    }
    public Iterator<Item> iterator()   // return an independent iterator over items in random order
    {
        return new RIterator();
    }
    
    private class RIterator implements Iterator<Item>{
        private Item[] iteratorValues;
        private int marker;
        
        public RIterator(){
            this.iteratorValues = (Item[]) new Object[N];
            
            for(int i=0;i<N;i++)
            {
                this.iteratorValues[i] = values[i];
                
            }
            StdRandom.shuffle(this.iteratorValues);
            this.marker =0;
        }
        
        public boolean hasNext(){
            
            return (this.marker<N);
        }
        
        public Item next(){
            if(this.marker<0 || this.marker>=N){
            throw new java.util.NoSuchElementException();
            }
            return this.iteratorValues[this.marker++];
        }
        
        public void remove(){
            throw new java.lang.UnsupportedOperationException();
        }



    }
}