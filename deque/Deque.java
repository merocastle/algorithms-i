
import java.util.Iterator;

public class Deque<Item> implements Iterable<Item> {
    
    private class Node<Item>{
        Item item;
        Node<Item> next;
        Node<Item> previous;
    }
    
    private Node<Item> first;
    
    private Node<Item> last;
    
    private int size_deque;   
    
    public Deque()                     // construct an empty deque
    {
      first = null;
      last = null;
     size_deque = 0;
    }
    
    public boolean isEmpty()           // is the deque empty?
    {
        return (size()==0) ;
    }
    
    public int size()                  // return the number of items on the deque
    {
        return size_deque;
    }
    
    public void addFirst(Item item)    // insert the item at the front
    {
        if(item == null){
            throw new java.lang.NullPointerException();
        }
        
        Node<Item> oldFirst = first;
        
        Node<Item> node = new Node<Item>();
        first = node;
        
        first.item = item;
        
        if(oldFirst == null){
            
            first.next = null;
                    
            last = first;
            
        }else{
           
            first.next = oldFirst;
            
            oldFirst.previous = first;
        }
        
        first.previous = null;
        
        size_deque++;    
    }
    
    public void addLast(Item item)     // insert the item at the end
    {
        if(item == null){
            throw new java.lang.NullPointerException();
        }
        
        Node<Item> oldLast = last;
        
        Node<Item> node = new Node<Item>();
        
        last = node;
        
        last.item = item;
        
        if(oldLast == null){
            
            last.next = null;
            
            last.previous = null;
            
            first = last;
            
        }else{
            
            oldLast.next = last;
            
            last.next = null;
            
            last.previous = oldLast;
            
        }
        
        size_deque++;
    }
  
    public Item removeFirst()          // delete and return the item at the front
    {
        if(isEmpty()){
            throw new java.util.NoSuchElementException();
        }
        
        
        
        Item item = first.item;
        
        first = first.next;
        
       
        
        if(first != null){
            
         first.previous = null;
         
        }
        
        if(first == null){
            
            last = first;
            
        }
        
        size_deque--;
        
        return item;
        
    }
    public Item removeLast()           // delete and return the item at the end
    {
         if(isEmpty()){
            throw new java.util.NoSuchElementException();
        }
                    
        Item item = last.item;  
        
        if(last.previous != null)
        {
         //means the deque has more than one item. delete the item
             last = last.previous;
             
             last.next = null;
             
        }else
        {
             first = last = null;
        }
         
        size_deque--;
        
        return item;
    }
    public Iterator<Item> iterator()   // return an iterator over items in order from front to end
    {
        
        return new DequeIterator();
        
    }
    
    private class DequeIterator implements Iterator<Item>
    {
        private Node<Item> current = first;
        
        
        public boolean hasNext()
        {
            return current != null;
        }
        
        public Item next()
        {
            if(current == null){
                throw new java.util.NoSuchElementException();
            }
            Item item = current.item;
            
            current = current.next;
            
            
            return item;
        }
        
        public void remove()
        {
            throw new java.lang.UnsupportedOperationException();
        }
    }
}